## Below I'm gonna try to document all the parts used / required / planned: (assembled list on 2020-08-22, prices valid for Aliexpress sale 2020-08-24 to 2020-08-29)

- €2 [EU Power Adapter casing](https://www.amazon.de/dp/B001BAQOM6) with switch (which we replace with small display):

- $1.61 [ESP8266-F](https://www.aliexpress.com/item/32324777806.html)  
  or $12.80 for 10 (=$1.28 per piece) [ESP8266-F](https://www.aliexpress.com/item/32728213900.html)
    - actually I'm planning to use ESP32 for future versions instead  
      (since I have 4 of those still lying around and also to be able to use them to relay battery-powered bluetooth sensors later on)  
      $4.71 for 2 (=$2.355 per piece) [ESP32](https://www.aliexpress.com/item/32808772590.html)  
      or $21.58 for 10 (=$2.158 per piece) [ESP32](https://www.aliexpress.com/item/32808546440.html)  
      <br/>
- $4.33 for 2 (=$2.165 per piece) [BME280](https://www.aliexpress.com/item/32817286611.html)  
  or $21.56 for 10 (=$2.156 per piece) [BME280](https://www.aliexpress.com/item/4001005725065.html)
- $1.44 [220V AC to 5V DC module](https://www.aliexpress.com/item/32831772101.html) or [here](https://www.aliexpress.com/item/32657634893.html)  
  or $2.69 for 2 (=$1.345 per piece) [220V AC to 5V DC modules](https://www.aliexpress.com/item/4000529515321.html)  
  or $4.70 for 5 (=$0.94 per piece) [220V AC to 5V DC modules](https://www.aliexpress.com/item/4000529540020.html)

- $1.85 for 50 (=$0.037 per piece) [AMS1117-3.3V](https://www.aliexpress.com/item/4000075698686.html)
- $1.67 for 20 (=$0.0835 per piece) [6.3V 47µF capacitors](https://www.aliexpress.com/item/4001042194301.html)

- $1.88 for 5 (=$0.376 per piece) [5V Relays](https://www.aliexpress.com/item/32885988222.html)  
  or $3.46 for 10 (=$0.346 per piece [5V Relays](https://www.aliexpress.com/item/32502467530.html)

- $4.37 for 50 (=$0.0874 per piece) [Relay driver NUD3160](https://www.aliexpress.com/item/4001115780535.html)  
  or alternatively assemble relay driver from transistor + diode + resistor (cheaper, but requires a bit more soldering):
    - $1.30 for 100 ($0.013 per piece) [Transistor BC548](https://www.aliexpress.com/item/33045062640.html) or [here](https://www.aliexpress.com/item/33041890499.html)
    - $0.91 for 100 ($0.0091 per piece) [Diodes N4007](https://www.aliexpress.com/item/4001272645647.html)
    - $1.05 for 100 ($0.0105 per piece) [Resistor 2.2kΩ](https://www.aliexpress.com/item/32853926318.html) or [here](https://www.aliexpress.com/item/4000096900604.html) or [here](https://www.aliexpress.com/item/4000695402017.html)
    <br/>
- $15.09 for 10 (=$1.509 per piece) [0.91" 128*32pixel white OLED displays](https://www.aliexpress.com/item/32961279307.html)

- $0.76 for 50 (=$0.0152 per piece) [push buttons slim](https://www.aliexpress.com/item/1068215251.html)

<br/>
<br/>
<br/>
<br/>

## below, some excluded links, found cheaper replacements, just leaving them here for the unlikely event of them becoming useful again:

- $5.43 for 50 (=$0.1086 per piece) [relay driver NUD3112](https://www.aliexpress.com/item/33048307265.html)  
- $1.28 for 100 ($0.0128 per piece) [Diodes N4007](https://www.aliexpress.com/item/33050898882.html)
- $1.22 for 100 ($0.0122 per piece) [Diodes N4007](https://www.aliexpress.com/item/32875934069.html)
- $1.07 for 100 ($0.0107 per piece) [Diodes N4007](https://www.aliexpress.com/item/32843733405.html)
- searching for 22µF capacitor
- $2.19 for 5 [10V 33µF capacitors](https://www.aliexpress.com/item/32900599422.html)
- $1.88 for 5 [400V 47µF capacitors](https://www.aliexpress.com/item/32908618939.html)
