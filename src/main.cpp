#include <Arduino.h>
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h> // mqtt client
#include <NTPClient.h> // time server lib for knowing date & time
#include <WiFiUdp.h> // required for time server lib
#include <time.h> // required for getTimeStampString() -> localtime

#include <Wire.h>
#include <Adafruit_BME280.h>

#include <U8g2lib.h> // 0,91" OLED LCD128x32
U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C u8g2(U8G2_R0); 

#include <Config.h>

#define LEDPIN 2
boolean ledToggle = false;

WiFiClient espClient;
PubSubClient mqttClient(espClient);

Adafruit_BME280 bme; // I2C
//Adafruit_BME280 bme(BME_CS); // hardware SPI

//#define BME_SCK 13
//#define BME_MOSI 12
//#define BME_CS 14
//#define BME_MISO 16
//Adafruit_BME280 bme(BME_CS, BME_MOSI, BME_MISO, BME_SCK); // software SPI

#define RELAYPIN 14

float temperature;
float humidity;
float pressure;

bool bme_found;

WiFiUDP ntpUDP;

unsigned long previousMillis = millis();
const unsigned int sensorInterval = 60000;

String wifiMac;

// You can specify the time server pool and the offset (in seconds, can be
// changed later with setTimeOffset() ). Additionaly you can specify the
// update interval (in milliseconds, can be changed using setUpdateInterval() ).
NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 7200, 60000);
//NTPClient timeClient(ntpUDP);

// https://github.com/arduino-libraries/NTPClient/issues/36
String getTimeStampString() {
  time_t rawtime = timeClient.getEpochTime();
  struct tm * ti;
  ti = localtime (&rawtime);

  uint16_t year = ti->tm_year + 1900;
  String yearStr = String(year);

  uint8_t month = ti->tm_mon + 1;
  String monthStr = month < 10 ? "0" + String(month) : String(month);

  uint8_t day = ti->tm_mday;
  String dayStr = day < 10 ? "0" + String(day) : String(day);

  uint8_t hours = ti->tm_hour;
  String hoursStr = hours < 10 ? "0" + String(hours) : String(hours);

  uint8_t minutes = ti->tm_min;
  String minuteStr = minutes < 10 ? "0" + String(minutes) : String(minutes);

  uint8_t seconds = ti->tm_sec;
  String secondStr = seconds < 10 ? "0" + String(seconds) : String(seconds);

  return yearStr + "-" + monthStr + "-" + dayStr + " " +
          hoursStr + ":" + minuteStr + ":" + secondStr;
}

void log(String msg) {
  String tMsg = getTimeStampString() + " " + msg;
  Serial.println("\n"+tMsg);
  if(mqttClient.connected())
    mqttClient.publish((wifiMac + ".info").c_str(), tMsg.c_str());
}

void init_wifi() {
    WiFi.mode(WIFI_STA);
    WiFi.begin(WIFI_SSID, WIFI_PW);
    wifiMac = WiFi.macAddress();
    wifiMac.replace(":","");

    log("Connecting to wifi");
    while (WiFi.status() != WL_CONNECTED) {
        delay(100);
        Serial.print(".");
        digitalWrite(LEDPIN, ledToggle); ledToggle=!ledToggle;
    }
    log("Wifi connected. IP = " + WiFi.localIP().toString());
}

void init_bme() {

    bme_found = bme.begin(0x77); // my BME280 has 0x76, default is 0x77.
    //bme_found = bme.begin(); // my BME680 has default 0x77.
    if(bme_found) {
        // for weather monitoring -- Weather Station Scenario --
        // Serial.println("forced mode, 1x temperature / 1x humidity / 1x pressure oversampling,");
        // Serial.println("filter off");
        // bme.setPressureOversampling(1);
        bme.setSampling(Adafruit_BME280::MODE_FORCED,
                        Adafruit_BME280::SAMPLING_X1, // temperature
                        Adafruit_BME280::SAMPLING_X1, // pressure
                        Adafruit_BME280::SAMPLING_X1, // humidity
                        Adafruit_BME280::FILTER_OFF   );
                        
        // suggested rate is 1/60Hz (1m)
        // delayTime = 60000; // in milliseconds
    }
    else {
        log("Could not find a valid BME280 sensor, check wiring!");
    }
}

void mqttReconnect() {
    digitalWrite(LEDPIN, LOW);

    log("Attempting to connect to MQTT server");
    String clientId = "ESP8266Client-" + wifiMac;
    if (mqttClient.connect(clientId.c_str(), MQTT_USER, MQTT_PASS)) {
        log("MQTT Connection established! My IP: " + WiFi.localIP().toString());
        mqttClient.subscribe((wifiMac + ".relay").c_str());
    } else {
        log("connecting to MQTT Server failed! rc=" + mqttClient.state());
    }

    digitalWrite(LEDPIN, HIGH);
}

void mqttCallback(char* topic, byte* payload, unsigned int length) {
    digitalWrite(LEDPIN, LOW);
    
    String topicString = String(topic);
    String payloadString = "";
    for(uint i=0 ; i<length ; i++)
        payloadString += (char) payload[i];

    log("got mqtt msg: " + topicString + " = " + payloadString);
    
    if(topicString == (wifiMac + ".relay")) {
        if(payloadString == "0") {  
            digitalWrite(RELAYPIN, LOW);
            log("turned off relay");
        }
        else if(payloadString == "1") {
            digitalWrite(RELAYPIN, HIGH);
            log("turned on relay");
        }
        else
            log("matched relay topic, but could not match 0 or 1 to turn it on or off?!");
    }
    else
        log("could not match relay topic, although it's the only one I've subscribed to:\n" + String(topic) + "\n!=\n" + (wifiMac + ".relay"));

    digitalWrite(LEDPIN, HIGH);
}

void readBme() {
    digitalWrite(LEDPIN, LOW);

    if(!bme_found) {
        init_bme();
    }

    if(bme_found) {
        // bme.performReading();
        bme.takeForcedMeasurement();

        temperature = bme.readTemperature();
        humidity = bme.readHumidity();
        pressure = bme.readPressure();

        Serial.println(getTimeStampString() + " temperature = " + String(temperature, 2));
        Serial.println(getTimeStampString() + " humidity = " + String(humidity, 2));
        Serial.println(getTimeStampString() + " pressure = " + String(pressure/100, 2));
        //Serial.println(getTimeStampString() + " gas = " + String(bme.gas_resistance, 0));

        mqttClient.publish((wifiMac + ".temperature").c_str(), String(temperature, 2).c_str());
        mqttClient.publish((wifiMac + ".humidity").c_str(), String(humidity, 2).c_str());
        mqttClient.publish((wifiMac + ".pressure").c_str(), String(pressure/100, 2).c_str());
        //mqttClient.publish((wifiMac + ".gas").c_str(), String(bme.gas_resistance, 0).c_str());

        u8g2.clearBuffer();         // clear the internal memory
        u8g2.setFont(u8g2_font_helvR14_tf);  // choose a suitable font at https://github.com/olikraus/u8g2/wiki/fntlistall
        u8g2.drawStr(0,15, (String(temperature, 1) + " C").c_str());
        u8g2.drawStr(61,15,String((((6.112*pow(2.7182818284,((17.67*temperature)/(temperature+243.5))))*humidity*18.02)/((273.15+temperature)*100*0.08314)),2).c_str());
        u8g2.drawCircle(38,3,2,U8G2_DRAW_ALL);
        u8g2.drawStr(0,32, (String(humidity, 1) + "%  " + String(((243.5*(log(humidity/100)+((17.67*temperature)/(243.5+temperature))))/(17.67-log(humidity/100)-((17.67*temperature)/(243.5+temperature)))),2) + " C").c_str());
        u8g2.drawCircle(109,20,2,U8G2_DRAW_ALL);
        u8g2.setFont(u8g2_font_5x7_tf);
        u8g2.drawStr(56,25,"T");
        u8g2.drawStr(57,32,"P");
        u8g2.drawLine(119,0,112,13);
        u8g2.setFont(u8g2_font_7x14_tr);
        u8g2.drawStr(108,7,"g");
        u8g2.setFont(u8g2_font_t0_17_tr);
        u8g2.drawStr(117,14,"m");
        u8g2.setFont(u8g2_font_micro_tr);
        u8g2.drawStr(125,5,"3");
        u8g2.sendBuffer();         // transfer internal memory to the display
    }
    else {
        log("Could not find BME280 sensor!");
        
        // blink fast to show theres a problem!
        for(size_t i = 0; i < 20; i++) {
            digitalWrite(LEDPIN, ledToggle); ledToggle=!ledToggle;
            delay(50);
        }
    }
    digitalWrite(LEDPIN, HIGH);
}

void setup() {
  pinMode(RELAYPIN, OUTPUT);
  digitalWrite(RELAYPIN, LOW);

  Serial.begin(115200);
  log("setup started, Serial.begin(115200); called.");

  pinMode(LEDPIN, OUTPUT);
  digitalWrite(LEDPIN, HIGH);
  log("initialized led control & turned it on");

  // see ~/.platformio/packages/framework-arduinoespressif8266/variants/nodemcu/pins_arduino.h
  // D1 = GIO5 = SCL = orange = logic analyzer CH1 = PulseView 0
  // D2 = GIO4 = SDA = brown/yellow = logic analyzer CH2 = PulseView 1
  Wire.begin(4, 5); 
  Wire.setClock(100000);
  log("I2C wire for pins 4+5 with Clockspeed 100000 initialized.");

  u8g2.begin();
  u8g2.clearBuffer();                   // clear the internal memory
  u8g2.setContrast(8);
  u8g2.setFont(u8g2_font_logisoso28_tr);// choose a suitable font at https://github.com/olikraus/u8g2/wiki/fntlistall
  u8g2.drawFrame(0,0,128,32);
  u8g2.drawStr(8,29,"Booting");         // write something to the internal memory
  u8g2.sendBuffer();					// transfer internal memory to the display
  log("0,91\" OLED LCD128x32 initialized to read \"Booting\" with frame showing display size");

  init_wifi();
  ArduinoOTA.begin();
  log("ArduinoOTA.begin(); called.");

  init_bme();

  timeClient.begin();
  timeClient.update();
  log("timeClient setup and synced.");

  mqttClient.setServer(MQTT_SERVER, MQTT_PORT);
  mqttClient.setCallback(mqttCallback);
  mqttReconnect();

  readBme();
  previousMillis = millis();
}

void loop() {
  Serial.println(getTimeStampString() + " loop begin");
  ArduinoOTA.handle();

  timeClient.update();
  if (!mqttClient.connected()) {
    mqttReconnect();
  }
  mqttClient.loop();

  unsigned long currentMillis = millis();
  if ((unsigned long)(currentMillis - previousMillis) >= sensorInterval )
  {
    previousMillis = currentMillis;
    readBme();
  }

  // digitalWrite(ledPin, ledToggle); ledToggle=!ledToggle;
  delay(1000);

  Serial.println(getTimeStampString() + " loop end");
}