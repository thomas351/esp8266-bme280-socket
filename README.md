# ESP8266 BME280 Socket

`include/Config.h.sample` needs to be copied to `include/Config.h` and filled with values that suite your environment (wifi + mqtt).

If you don't have an MQTT-server yet, I can recommend [IO-Broker](https://www.iobroker.net/docu/index-378.htm), which is what I'm using.

In the docs folder you can find a [hardware part's list](docs/parts_list.md) with prices and links to shops that sell them.

I'm planning to add a few [push buttons](https://www.aliexpress.com/item/1068215251.html) when they get deivered and program a menu to choose what's displayed and enable switching the relay on and off directly on the device in addition to through the MQTT-server.

I made this wiring diagram of my hardware:
![wiring diagram](docs/wiring_diagram_2020-08-23.png)

And here's a photo of my current version of my hardware for this project:
![Image of my current hardware](docs/current_hardware_2020-08-22.jpg)